open EduATypes;

/* ------------------------------------------------------------------ */
/* Types used to parse/save. */

type tableName = string;
type cells = array(string);

type lineNumber = int;

type parserMode =
  | NeedTableLine
  | NeedDataLine((tableName, array((int, cells))));

type lineType =
  | NotImportant
  | Table(tableName)
  | Data(cells);

type cellValue =
  | Null
  | Int(int)
  | Str(string)
  | NotAValue(lineNumber);

type cellType =
  | CInt
  | CStr
  | CEnum( array(string) );

type tableRow = (lineNumber, array(cellValue));
type table = (tableName, array(tableRow));

type parsedRow('a) =
  | Row('a)
  | ParseError(string);

type parsedScenario = {
  pproducts : option(array(product)),
  passets : option(array(assetType)),
  pactions : option(array(action)),
  poperations : option(array(operation)),
  pmodes : option(array(mode)),
  presources : option(array(resource)),
  pmachines : option(array(machine)),
  pbuffers : option(array(buffer)),
  pworkstations : option(array(workstation)),
  pconnections : option(array(connection))
};

type parseState = (parsedScenario, array(string));
exception ParseException(array(string));

