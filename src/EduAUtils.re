open EduATypes;

/* ------------------------------------------------------------------ */
/* The option monad. */

  /* pass */
let (|>) = (x, f) =>
  switch (x) {
    | None => None
    | Some(v) => f(v)
  };

  /* collect */
let (||>) = (x, f) =>
  switch (x) {
    | None => None
    | Some(v) =>
        switch ( f(v) ) {
          | None => None
          | Some(y) => {
              let result = (v, y);
              Some(result);
            }
        }
  };

  /* return passed value, or... */
let (|=>) = (x : option('a), default : 'a) : 'a =>
  switch (x) {
    | None => default
    | Some(v) => v
  };


/* ------------------------------------------------------------------ */
/* The simulation monad. */

let ($>) = (s, f) => f(s);

let ($=>) = (s, value) => (value, s);


/* ------------------------------------------------------------------ */
/* Other utility functions. */

let enoughResources = (s : simulation, arr : array(resourceInfo)) : bool => {
  let resources = Array.to_list(s.scenario.resources);
  let howManyOfRes = (i : resourceId, asts : list(resource)) : int => {
    let rec loop = (lst : list(resource)) : int =>
      switch (lst) {
        | [] => 0
        | [x, ...rest] =>
            switch (x.id) {
              | i => x.available
              | _ => loop(rest)
            }
      };

    loop(asts);
  };

  let rec loop = (lst : list(resourceInfo)) : bool =>
    switch (lst) {
      | [] => true
      | [x, ...rest] => {
          let (a_id, required) = x;
          let available = howManyOfRes(a_id, resources);
          if ( required > available ) {
            false;
          } else {
            loop(rest);
          };
        }
    };

  loop( Array.to_list(arr) );
};

let howMuchOfAsset = (id : assetId, arr : array(assetQty)) : int =>
  Js.Array.reduce(
    (acc, info) => {
      let (aid, qty) = info;
      if ( id == aid ) {
        qty;
      } else {
        acc;
      };
    },
    0,
    arr
  );

let enoughAssets = (s : simulation, arr : array(assetQty)) : bool => {
  let rec loop = (lst : list(assetQty)) : bool =>
    switch (lst) {
      | [] => true
      | [x, ...rest] => {
          let (a_id, required) = x;
          let available = howMuchOfAsset(a_id, s.assets);
          if ( required > available ) {
            false;
          } else {
            loop(rest);
          };
        }
    };

  loop( Array.to_list(arr) );
};


let readyMode = (s : simulation, m : machine, mm : machineMode, mode : option(machineMode)) : bool =>
  switch (mode) {
    | None => { /* all the resources are needed */
        let resources = Js.Array.concat( m.resources_info, mm.resources_info );
        enoughResources(s, resources);
      }

    | Some(set) => { /* machine-specific resources are already taken */
        let released_res = set.resources_info;
        let resources1 = Js.Array.map(
          (rinfo) => {
            let (r_id, rqty) = rinfo;
            let to_check = Js.Array.filter(
              (x) => {
                let (id, _) = x;
                id == r_id;
              },
              released_res
            );
            switch (to_check) {
              | [||] => (r_id, rqty)
              | _ => {
                  let (_, qty) = to_check[0];
                  let diff = rqty - qty;
                  if ( diff > 0 ) {
                    (r_id, diff);
                  } else {
                    (r_id, 0);
                  };
                }
            };
          },
          mm.resources_info
        );
        let resources = Js.Array.filter(
          (x) => {
            let (_, qty) = x;
            qty > 0;
          },
          resources1
        );

        enoughResources(s, resources);
      }
  };


let operationInMode = (id : operationId, ws : (workstation, mode)) : option((workstation, mode)) => {
  let (_, mode) = ws;
  Js.Array.find(
    (ot : operationIdTime) => {
      let (op_id, _) = ot;
      op_id == id;
    },
    mode.info
  )
  |> ( (_) => Some(ws) );
};


/* idQty-related functions */

let normIdQty = (arr : array(idQty)) : array(idQty) => {
  let rec loop = (i, ar) =>
    if ( i == Array.length(ar) ) {
      ar;
    } else {
      let elm = ar[i];
      let (id, _) = elm;
      let same_ids =
        Js.Array.filter(
          (e) => {
            let (e_id, _) = e;
            e_id == id;
          },
          ar
        );
      if ( Js.Array.length(same_ids) == 1 ) {
        loop(i+1, ar);
      } else {
        let other_ids =
          Js.Array.filter(
            (e) => {
              let (e_id, _) = e;
              e_id != id;
            },
            ar
          );
        let sum =
          Js.Array.reduce(
            (acc, e) => {
              let (_, qty) = e;
              acc + qty;
            },
            0,
            same_ids
          );
        ignore( Js.Array.push( (id, sum), other_ids) );
        loop(0, other_ids);
      };
    };

  let tmp_arr = loop(0, arr);
  Js.Array.filter(
    (e : idQty) => {
      let (_, qty) = e;
      qty != 0;
    },
    tmp_arr
  );
};

let addIdQty = (a1 : array(idQty), a2 : array(idQty)) : array(idQty) => {
  let a3 = Js.Array.concat(a1, a2);
  normIdQty(a3);
};

let subIdQty = (a1 : array(idQty), a2 : array(idQty)) : array(idQty) => {
  let a3 =
    Js.Array.map(
      (e) => {
        let (id, qty) = e;
        (id, -qty);
      },
      a2
    );
  addIdQty(a1, a3);
};

let isNegativeIdQty = (arr : array(idQty)) : bool =>
  Js.Array.some(
    (e) => {
      let (_, qty) = e;
      qty < 0;
    },
    arr
  );
