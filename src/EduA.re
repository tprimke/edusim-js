open EduATypes;
open EduAUtils;
open EduACrud;

let parse = EduAParser.parse;

let parseTables = EduAParser.parseTables;

let newSim = (s : scenario) => {
  
  let initial_asset = (a : assetType) : assetQty => (a.id, a.initial);

  { scenario: s,
    assets: Js.Array.map( initial_asset, s.assets ),
    handlers: [||],
    next_id: 1,
    time: 0 }
};


/*
   ------------------------------------------------------------------
   -- Info functions
   ------------------------------------------------------------------
*/

let infoOfResource = (r : resource) : rInfo =>
  rInfo( ~id = r.id, ~name = r.name, ~available = r.available, ~operation_time = r.operation_time);

let infoOfProductQty = (pb : productQty) : pbInfo => {
  let (id, qty) = pb;
  pbInfo( ~product_id = id, ~qty = qty );
};

let infoOfResourceQty = (rq : resourceQty) : rSimpleInfo => {
  let (id, qty) = rq;
  rSimpleInfo( ~resource_id = id, ~qty = qty );
};


let infoOfBuffer = (b : buffer) : bInfo => {
  let pb_info = Js.Array.map( infoOfProductQty, b.products );
  bInfo( ~id = b.id, ~name = b.name, ~products = pb_info );
};

let assetsInfo = (s : simulation) : array(aInfo) => {
  let sim_assets = s.assets;
  let scen_assets = s.scenario.assets;
  let isAssetId = (a : assetType, aq : assetQty) => {
    let (id, _) = aq;
    a.id == id;
  };
  let to_ainfo = (a : assetType) : aInfo => {
    let the_assets = Js.Array.filter( (x) => isAssetId(a, x), sim_assets );
    let (_, qty) = the_assets[0];
    aInfo( ~id = a.id, ~name = a.name, ~value = qty );
  };

  Js.Array.map( to_ainfo, scen_assets );
};

let infoOfWorkstation = (w : workstation) : wInfo => {
  let mode = switch (w.machine_mode) {
    | None => Js.Nullable.null
    | Some(mode_id) => Js.Nullable.return(mode_id)
  };
  let r_info = Js.Array.map( (r : resourceQty) => infoOfResourceQty(r), w.resources );

  switch (w.status) {
    | NotReady => 
        wInfo(
          ~id = w.id,
          ~name = w.name,
          ~resources = r_info,
          ~buffer_id = w.buffer_id,
          ~machine_mode = mode,
          ~status = "not ready",
          ()
        )

    | Setup(time, total_time, mode_id) =>
        wInfo(
          ~id = w.id,
          ~name = w.name,
          ~resources = r_info,
          ~buffer_id = w.buffer_id,
          ~machine_mode = mode,
          ~status = "setup",
          ~mode_id = mode_id,
          ~time = time,
          ~totalTime = total_time,
          ()
        )

    | Idle =>
        wInfo(
          ~id = w.id,
          ~name = w.name,
          ~resources = r_info,
          ~buffer_id = w.buffer_id,
          ~machine_mode = mode,
          ~status = "idle",
          ()
        )

    | Operation(time, total_time, op_id) =>
        wInfo(
          ~id = w.id,
          ~name = w.name,
          ~resources = r_info,
          ~buffer_id = w.buffer_id,
          ~machine_mode = mode,
          ~status = "operation",
          ~operation_id = op_id,
          ~time = time,
          ~totalTime = total_time,
          ()
        )

  };
};

let info = (s : simulation) : simulationInfo => {
  let r_info = Js.Array.map( infoOfResource, s.scenario.resources );
  let b_info = Js.Array.map( infoOfBuffer, s.scenario.buffers );
  let a_info = assetsInfo(s);
  let w_info = Js.Array.map( infoOfWorkstation, s.scenario.workstations );

  simulationInfo( 
    ~resources = r_info,
    ~buffers = b_info,
    ~assets = a_info,
    ~workstations = w_info );
};

let infoOfAction = (a : action) : jsActionInfo => {
  let r_info = (typ : string, v : (int, int)) : actionReqResInfo => {
    let (id, qty) = v;
    actionReqResInfo( ~pa = typ, ~id = id, ~qty = qty );
  };

  switch (a.info) {
    | OnlyAssets(i) => {
        let reqs = Js.Array.map( (x) => r_info("asset", x), i.asset_reqs );
        let ress = Js.Array.map( (x) => r_info("asset", x), i.asset_results );
        
        jsActionInfo(
          ~id = a.id,
          ~name = a.name,
          ~requirements = reqs,
          ~results = ress,
          ()
        );
      }

    | ProductsAndAssets(i) => {
        let areqs = Js.Array.map( (x) => r_info("asset", x), i.asset_reqs );
        let aress = Js.Array.map( (x) => r_info("asset", x), i.asset_results );
        let preqs = Js.Array.map( (x) => r_info("product", x), i.product_reqs );
        let press = Js.Array.map( (x) => r_info("product", x), i.product_results );

        jsActionInfo(
          ~id = a.id,
          ~name = a.name,
          ~buffer_id = i.buffer_id,
          ~requirements = Js.Array.concat(areqs, preqs),
          ~results = Js.Array.concat(aress, press),
          ()
        );
      }
  };
};

let infoOfTransport = (c : connection) : jsTransportInfo =>
  switch (c.constraints) {
    | None =>
        jsTransportInfo(
          ~id = c.id,
          ~buffer_from = c.from_buffer,
          ~buffer_to = c.to_buffer,
          ()
        )

    | Some(arr) =>
        jsTransportInfo(
          ~id = c.id,
          ~buffer_from = c.from_buffer,
          ~buffer_to = c.to_buffer,
          ~products = arr,
          ()
        )
  };

let allActions = (s : simulation) : array(jsActionInfo) =>
  Js.Array.map( infoOfAction, s.scenario.actions );

let readyActions = (s : simulation) : array(actionId) => {
  let r_actions = Js.Array.filter( (a) => actionReady(s, a), s.scenario.actions );
  Js.Array.map( (a : action) => a.id, r_actions );
};

let allTransports = (s : simulation) : array(jsTransportInfo) =>
  Js.Array.map( infoOfTransport, s.scenario.connections );

let readyTransports = (s : simulation) : array(jsTransportInfo) => {
  let r_transports = [||];

  Js.Array.forEach(
    (c) => {
      let products = [||];
      switch ( bufferOfSimulation(s, c.from_buffer) ) {
        | None => ()
        | Some(buffer) => {
            Js.Array.forEach(
              (pqty) => {
                let (p_id, _) = pqty;
                if ( transportReady(s, c, p_id) ) {
                  Js.Array.push( p_id, products );
                  ();
                } else {
                  ();
                };
              },
              buffer.products
            );
          }
      };
      if ( Js.Array.length(products) > 0 ) {
        Js.Array.push(
          jsTransportInfo(
            ~id = c.id,
            ~buffer_from = c.from_buffer,
            ~buffer_to = c.to_buffer,
            ~products = products,
            ()
          ),
          r_transports
        );
        ();
      } else {
        ();
      };
    },
    s.scenario.connections
  );

  r_transports;
};

let infoOfResource = (info : resourceInfo) : jsResourceInfo => {
  let (id, qty) = info;
  jsResourceInfo(
    ~id = id,
    ~qty = qty
  );
};

let infoOfReqRes = (info : reqOrRes) : jsReqOrResInfo => {
  let (i, qty) = info;
  switch (i) {
    | Product(id) =>
        jsReqOrResInfo(
          ~id = id,
          ~type_ = "product",
          ~qty = qty
        )

    | Asset(id) =>
        jsReqOrResInfo(
          ~id = id,
          ~type_ = "asset",
          ~qty = qty
        )
  };
};

let infoOfOperation = (s : simulation, info : operationIdTime) : jsOperationInfo => {
 let (op_id, time) = info;
 switch ( operationOfSimulation(s, op_id) ) {
    | None =>
        jsOperationInfo(
          ~id = op_id,
          ~time = -1,
          ~requirements = [||],
          ~results = [||]
        )

    | Some(op) => {
        let reqs = Js.Array.map( infoOfReqRes, op.info.reqs );
        let ress = Js.Array.map( infoOfReqRes, op.info.results );
        jsOperationInfo(
          ~id = op_id,
          ~time = time,
          ~requirements = reqs,
          ~results = ress
        );
      }
  };
};

let infoOfMode = (s : simulation, mode : machineMode) : jsModeOperationsInfo => {
  let resources = Js.Array.map( infoOfResource, mode.resources_info );
  switch ( modeOfSimulation(s, mode.mode_id) ) {
    | None =>
        jsModeOperationsInfo(
          ~id = mode.mode_id,
          ~time = mode.time,
          ~resources = resources,
          ~operations = [||]
        )

    | Some(oset) => {
        let operations = Js.Array.map( (x : operationIdTime) => infoOfOperation(s, x), oset.info );
        jsModeOperationsInfo(
          ~id = mode.mode_id,
          ~time = mode.time,
          ~resources = resources,
          ~operations = operations
        );
      }
  };
};

let noWorkstationInfo =
      jsWorkstationInfo(
        ~resources = [||],
        ~modes = [||]
      );

let workstationInfo = (s : simulation, id : workstationId) : jsWorkstationInfo =>
  switch ( workstationOfSimulation(s, id) ) {
    | None => noWorkstationInfo

    | Some(w) =>
        switch ( machineOfSimulation(s, w.machine_id) ) {
          | None => noWorkstationInfo
          
          | Some(m) => {
              let resources = Js.Array.map( infoOfResource, m.resources_info );
              let modes = Js.Array.map( (x : machineMode) => infoOfMode(s, x), m.mode_info );
              jsWorkstationInfo(
                ~resources = resources,
                ~modes = modes
              );
            }
        }
  };

let readyModes = (s : simulation, id : workstationId) : array(modeId) =>
  /* get the workstation */
  workstationOfSimulation(s, id)

      /* get the machine (and its modes...) */
  ||> ( (w) => machineOfSimulation(s, w.machine_id) )

      /* find the current workstation mode, basing on the workstation's state */
  ||> ( (wm) => {
          let (w, m) = wm;
          switch (w.status) {
            | NotReady => Some(None)
            | Setup(_, _, w_mode) => Some( modeOfMachine(m, w_mode) )
            | _ =>
                switch (w.machine_mode) {
                  | None => Some(None) /* the impossible case... */
                  | Some(w_mode) => Some( modeOfMachine(m, w_mode) )
                }
          };
        } )
  
     /* check, which modes are ready for the workstation */
  |> ( (wms) => {
          let ((_w, m), mm) = wms;

          /* the machine modes to check */
          let modes_to_check = 
            switch (mm) {
              | None => m.mode_info
              | Some(current_mode) =>
                  Js.Array.filter( (x : machineMode) => x.mode_id != current_mode.mode_id, m.mode_info )
            };

          /* check the modes */
          let modes = Js.Array.filter( (x : machineMode) => readyMode(s, m, x, mm), modes_to_check );
          Some( Js.Array.map( (x : machineMode) => x.mode_id, modes ) );
        } )
  |=> [||];

/* Returns Some(ws, op_set), when the workstation is Idle. Otherwise, returns None. */
let wsOpSet = (s : simulation, id : workstationId) : option( (workstation, mode) ) =>
  workstationOfSimulation(s, id)
  ||> ( (w) =>
         switch ((w.status, w.machine_mode)) {
           | (Idle, Some(mode_id)) => modeOfSimulation(s, mode_id)

           | _ => None
         } );

let bufferOfWorkstation = (s : simulation, ws : (workstation, mode)) : option(buffer) => {
  let (w, _) = ws;
  bufferOfSimulation(s, w.buffer_id);
};

let readyOperations = (s : simulation, id : workstationId) : array(operationId) =>
  wsOpSet(s, id)
  ||> bufferOfWorkstation(s)
  |> ( (wsb) => {
          let ((w, set), b) = wsb;
          Some( Js.Array.reduce(
              (acc, op_info) => {
                let (op_id, _) = op_info;
                switch ( readyOperation(s, op_id, b) ) {
                  | true => Js.Array.append(op_id, acc)
                  | false => acc
                };
              },
              [||],
              set.info
            )
          );
       } )
  |=> [||];

let execAction = (s : simulation, id : actionId) : (bool, simulation) =>
  /* get the action */
  actionOfSimulation(s, id)

  /* check whether the action is ready */
  |> ( (a) =>
          if ( actionReady(s, a) ) {
            Some(a);
          } else {
            None;
          } )

  /* check the action handlers */
  |> ( (a) => {
         let info_assets = assetsInfo(s);
         let dummy_buffer = bInfo( ~id = -1, ~name = "", ~products = [||] );
         let info_buffer =
           switch (a.info) {
             | OnlyAssets(_) => None
             | ProductsAndAssets(paa) =>
                 Some(
                   bufferOfSimulation(s, paa.buffer_id)
                   |> ( (b) => Some( infoOfBuffer(b) ) )
                   |=> dummy_buffer
                 )
           };
         let result =
           Js.Array.some(
             (h : handler) => {
               let (_, fnc) = h;
               switch (fnc) {
                 | BeforeAction(a_id, f) =>
                     if ( a_id == id ) {
                       ! f(info_assets, info_buffer, id);
                     } else {
                       false
                     }
                 | _ => false
               }
             },
             s.handlers
           );
         if ( result ) {
           None;
         } else {
           Some(a);
         }
       } )

  /* get all the needed assets and products
     produce all the result assets and products
     be happy! */
  |> ( (a) =>
         Some(
         switch (a.info) {
           | OnlyAssets(info) =>
               s
               $> takeFromAssets(info.asset_reqs)
               $> addToAssets(info.asset_results)
               $=> true

           | ProductsAndAssets(info) =>
               s
               $> takeFromAssets(info.asset_reqs)
               $> takeFromBuffer(info.buffer_id, info.product_reqs)
               $> addToAssets(info.asset_results)
               $> addToBuffer(info.buffer_id, info.product_results)
               $=> true
         }
         ) )

  /* in case of some failure, return false */
  |=> (false, s);

let startOperation = (s : simulation, w_id : workstationId, op_id : operationId) : (bool, simulation) =>
  /* get the workstation, and if it is Idle, then get the operations_set */
  wsOpSet(s, w_id)

  /* check, whether the operation belong to the set */
  |> operationInMode(op_id)
  
  /* get the workstation's buffer */
  ||> bufferOfWorkstation(s)

  /* is the operation ready? */
  |> ( (wsb) => {
         let ((w, set), b) = wsb;
         if ( readyOperation(s, op_id, b) ) {
           let result = (w, set, b);
           Some(result);
         } else {
           None
         }
       } )

  /* get the operation */
  |> ( (wsb) => {
         let (w, set, b) = wsb;
         switch ( operationOfSimulation(s, op_id) ) {
           | None => None
           | Some(o) => {
               let result = (w, set, b, o);
               Some(result);
             }
         };
       } )

  /* start the operation */
  |> ( (wsbo) => {
         let (w, set, b, o) = wsbo;
         let (asset_reqs, product_reqs) = operationReqs(o);

         let result =
           s
           $> takeFromAssets(asset_reqs)
           $> takeFromBuffer(b.id, product_reqs)
           $> simStartOperation(w, set, o)
           $=> true;

         Some(result);
       } )

  /* in case of some failure, return false */
  |=> (false, s);

let assignResource = (s : simulation, wid : workstationId, rid : resourceId, qty : int) : (bool, simulation) =>
  /* check the number of available resources */
  checkSimResources(s, rid, qty)

  /* get the workstation */
  |> ( (_) => workstationOfSimulation(s, wid) )

  /* assign the resources */
  |> ( (w) => {
         let result =
           s
           $> takeFromResources(rid, qty)
           $> assignResToWorkstation(w, rid, qty)
           $=> true;

         Some(result);
       }
     )

  /* in case of some failre, return false */
  |=> (false, s);

let releaseResource = (s : simulation, wid : workstationId, rid : resourceId, qty : int) : (bool, simulation) =>
  /* get the workstation */
  workstationOfSimulation(s, wid)

  /* check the number of assigned resources */
  |> checkWorkstationResources(rid, qty)

  /* release the resources */
  |> ( (w) => {
         let result =
           s
           $> releaseResFromWorkstation(w, rid, qty)
           $> updateWorkstationMode(wid)
           $> addToResources(rid, qty)
           $=> true;

         Some(result);
       }
     )

  /* in case of some failure, return false */
  |=> (false, s);

let startMode = (s : simulation, wid : workstationId, mid : modeId) : (bool, simulation) =>
  /* logic:
       - get the workstation (no workstation = false)
       - check, whether the mode is valid for the workstation
       - check the resources assigned to the workstation
         not enough resources = cannot initiate the mode change
       - change the status of the wokstation
           - stop doing any set up
           - stop doing any operation
           - start the mode change */
  /* get the workstation */
  workstationOfSimulation(s, wid)

  /* check, whether the mode is valid for the workstation */
  |> checkWorkstationValidMode(s, mid)

  /* check the resources assigned to the workstation */
  |> checkWorkstationResourcesForMode(s, mid)

  /* set the new status of the workstation */
  |> startWorkstationMode(s, mid)

  /* in case of some failure, return false */
  |=> (false, s);

let transfer = (s : simulation, c_id : connectionId, p_id : productId, qty : int) : (bool, simulation) =>
  /* get the connection */
  connectionOfSimulation(s, c_id)

  /* get the buffers */
  |> ( (c) => {
         let from_buffer = bufferOfSimulation(s, c.from_buffer);
         let to_buffer = bufferOfSimulation(s, c.to_buffer);
         switch (from_buffer, to_buffer) {
           | (Some(src), Some(dst)) => Some( (c, src, dst) )
           | _ => None
         };
       } )

  /* check the connection constraints */
  |> ( (cft) =>
         switch (cft) {
           | (c, from, _) =>
               switch (c.constraints) {
                 | None => Some(cft)
                 | Some(arr) => {
                     let any =
                       Js.Array.some(
                         (x : productQty) => {
                            let (pid, _) = x;
                            pid == p_id;
                         },
                         from.products
                       );
                     if ( any ) {
                       Some(cft);
                     } else {
                       None;
                     }
                   }
               }
           
           | _ => None
         } )

  /* check the product in the source buffer */
  |> ( (cft) =>
         switch (cft) {
           | (_, from, _) => {
               let to_subtract = [| (p_id, qty) |];
               let diff = subIdQty(from.products, to_subtract);
               if ( isNegativeIdQty(diff) ) {
                 None;
               } else {
                 Some(cft);
               }
             }

           | _ => None
         } )

  /* transfer the product */
  |> ( (cft) => 
         switch (cft) {
           | (c, from, to_) => {
               let change = [| (p_id, qty) |];
               let result =
                 s
                 $> takeFromBuffer(from.id, change)
                 $> addToBuffer(to_.id, change)
                 $=> true;

               Some(result);
             }

           | _ => None
         } )

  /* in case something went wrong */
  |=> (false, s);

let tick = (s : simulation) : simulation =>
  Some(s)

  /* tick all the workstations */
  |> tickWorkstations

  /* in case of failure, return the unmodified simulation */
  |=> s;

let removeHandler = (s : simulation, hid : handlerId) : (bool, simulation) =>
  s
  $> simRemoveHandler(hid)
  $=> true;

let addBeforeAction = (s : simulation, aid : actionId, h : handlerBeforeBufferId) : (bool, handlerId, simulation) =>
  actionOfSimulation(s, aid)
  |> ( (_a) => {
         let new_handler = BeforeAction(aid, h);
         let (handler_id, new_sim) = simAddHandler(s, new_handler);
         Some( (true, handler_id, new_sim) );
       } )

  /* fallback, in case of some failure */
  |=> (false, -1, s);

type typeResult =
  | Workstation(workstationId)
  | Buffer;

let visInfo = (s : simulation) : array(jsVisInfo) => {

  /* returns the type of the requested buffer */
  let bufferType = (id : bufferId) : typeResult => {
    let wsts =
      Js.Array.filter(
        (w) => w.buffer_id == id,
        s.scenario.workstations
      );
    if ( Js.Array.length(wsts) == 1 ) {
      Workstation(wsts[0].id)
    } else {
      Buffer
    };
  };

  let inOuts = (id : bufferId) : ( array(visConnection), array(visConnection) ) =>
    Js.Array.reduce(
      (acc, conn) => {
        let { from_buffer, to_buffer } = conn;
        if ( id == from_buffer ) {
          let (ins, outs) = acc;
          let (out_type, the_id) =
            switch ( bufferType(to_buffer) ) {
              | Workstation(wid) => ("workstation", wid)
              | Buffer => ("buffer", to_buffer)
            };
          let new_out = visConnection( ~type_ = out_type, ~id = the_id );
          ignore( Js.Array.push( new_out, outs ) );
          (ins, outs);
        } else {
          if ( id == to_buffer ) {
            let (ins, outs) = acc;
            let (in_type, the_id) =
              switch ( bufferType(from_buffer) ) {
                | Workstation(wid) => ("workstation", wid)
                | Buffer => ("buffer", from_buffer)
              };
            let new_in = visConnection( ~type_ = in_type, ~id = the_id );
            ignore( Js.Array.push( new_in, ins ) );
            (ins, outs);
          } else {
            acc
          }
        };
      },
      ([||], [||]),
      s.scenario.connections
    );

  let arr_buf =
    Js.Array.reduce(
      (acc, b : buffer) =>
        switch (b.vis) {
          | None => acc
          | Some(v_info) => {
              let (ins, outs) = inOuts(b.id);
              let info = jsVisInfo(
                ~type_ = "buffer",
                ~id = b.id,
                ~x = v_info.x,
                ~y = v_info.y,
                ~inputs = ins,
                ~outputs = outs
              );
              ignore( Js.Array.push(info, acc) );
              acc;
            }
        },
      [||],
      s.scenario.buffers
    );

  let arr_wst =
    Js.Array.reduce(
      (acc, b : workstation) =>
        switch (b.vis) {
          | None => acc
          | Some(v_info) => {
              let (ins, outs) = inOuts(b.buffer_id);
              let info = jsVisInfo(
                ~type_ = "workstation",
                ~id = b.id,
                ~x = v_info.x,
                ~y = v_info.y,
                ~inputs = ins,
                ~outputs = outs
              );
              ignore( Js.Array.push(info, acc) );
              acc;
            }
        },
      [||],
      s.scenario.workstations
    );

  Js.Array.concat(arr_buf, arr_wst);
};

