open EduATypes;
open EduAUtils;

let connectionOfSimulation = (s : simulation, id : connectionId) : option(connection) =>
  Js.Array.find(
    (c : connection) => c.id == id,
    s.scenario.connections
  );

let bufferOfSimulation = (s : simulation, id : bufferId) : option(buffer) =>
  Js.Array.find(
    (b : buffer) => b.id == id,
    s.scenario.buffers
  );

let resourceOfSimulation = (s : simulation, id : resourceId) : option(resource) =>
  Js.Array.find(
    (r : resource) => r.id == id,
    s.scenario.resources
  );

let actionOfSimulation = (s : simulation, id : actionId) : option(action) =>
  Js.Array.find(
    (a : action) => a.id == id,
    s.scenario.actions
  );

let workstationOfSimulation = (s : simulation, id : workstationId) : option(workstation) =>
  Js.Array.find(
    (w : workstation) => w.id == id,
    s.scenario.workstations
  );

let machineOfSimulation = (s : simulation, id : machineId) : option(machine) =>
  Js.Array.find(
    (m : machine) => m.id == id,
    s.scenario.machines
  );

let modeOfMachine = (m : machine, id : modeId) : option(machineMode) =>
  Js.Array.find(
    (mm : machineMode) => mm.mode_id == id,
    m.mode_info
  );

let modeOfSimulation = (s : simulation, id : modeId) : option(mode) =>
  Js.Array.find(
    (os : mode) => os.id == id,
    s.scenario.modes
  );

let operationTimeOfMode = (s : mode, op_id : operationId) : option(time) =>
  Js.Array.reduce(
    (acc, x : operationIdTime) => 
      switch (acc) {
        | Some(q) => Some(q)
        | None => {
            let (id, time) = x;
            if ( id == op_id ) {
              Some(time);
            } else {
              acc;
            };
          }
      },
    None,
    s.info
  );

let operationOfSimulation = (s : simulation, id : operationId) : option(operation) =>
  Js.Array.find(
    (o : operation) => o.id == id,
    s.scenario.operations
  );

let howManyProducts = (id : productId, arr : array(productQty)) : int =>
  Js.Array.reduce(
    (acc, info) => {
      let (p_id, qty) = info;
      if ( p_id == id ) {
        qty;
      } else {
        acc;
      };
    },
    0,
    arr
  );

let changeAssets = (f : (int => int => int), arr : array(assetQty), s : simulation) : simulation => {
  let new_assets =
    Js.Array.map(
      (a_info) => {
        let (a_id, available) = a_info;
        let to_take = howMuchOfAsset(a_id, arr);
        if ( to_take == 0 ) {
          a_info;
        } else {
          (a_id, f(available, to_take));
        };
      },
      s.assets
    );

  { ...s, assets: new_assets };
};

let takeFromAssets = (arr : array(assetQty), s : simulation) : simulation =>
  changeAssets( (-), arr, s );

let addToAssets = (arr : array(assetQty), s : simulation) : simulation =>
  changeAssets( (+), arr, s );

let changeBuffer = (buffer_id : bufferId, arr : array(productQty), f : (array(productQty) => array(productQty) => array(productQty)), s : simulation) : simulation =>
  bufferOfSimulation(s, buffer_id)
  |> ( (b) => {
         let new_products = f(b.products, arr);
         let new_buffer = { ...b, products: new_products };

         /* change the old buffer with the new one in scenario */
         let new_buffers =
           Js.Array.map(
             (buf : buffer) =>
               if ( buf.id == new_buffer.id ) {
                 new_buffer;
               } else {
                 buf;
               },
             s.scenario.buffers
           );
         let new_scenario = { ...s.scenario, buffers: new_buffers };

         /* prepare the new simulation */
         Some( { ...s, scenario: new_scenario } );
       } )
  |=> s;

let takeFromBuffer = (id : bufferId, arr : array(productQty), s : simulation) : simulation =>
  changeBuffer(id, arr, subIdQty, s);

let addToBuffer = (id : bufferId, arr : array(productQty), s : simulation) : simulation =>
  changeBuffer(id, arr, addIdQty, s);

let operationInfo = (o : operation, arr : array(reqOrRes)) : (array(assetQty), array(productQty)) =>
  Js.Array.reduce(
    (acc, r) => {
      let (areq, preq) = acc;
      switch (r) {
        | (Product(id), qty) => {
            let new_preq = Js.Array.concat( preq, [| (id, qty) |] );
            (areq, new_preq);
          }

        | (Asset(id), qty) => {
            let new_areq = Js.Array.concat( areq, [| (id, qty) |] );
            (new_areq, preq);
          }
      };
    },
    ([||], [||]),
    arr
  );

let operationReqs = (o : operation) : (array(assetQty), array(productQty)) =>
  operationInfo(o, o.info.reqs);

let operationResults = (o : operation) : (array(assetQty), array(productQty)) =>
  operationInfo(o, o.info.results);

let substWorkstation = (w : workstation, s : simulation) : simulation => {
  let new_workstations =
    Js.Array.map(
      (wst : workstation) =>
        if ( wst.id == w.id ) {
          w;
        } else {
          wst;
        },
      s.scenario.workstations
    );
  let new_scenario = { ...s.scenario, workstations: new_workstations };
  { ...s, scenario: new_scenario };
};

let substWorkstationStatus = (w : workstation, st : workstationStatus, s : simulation) => {
  let new_workstation = { ...w, status: st };
  substWorkstation(new_workstation, s);
};

let simStartOperation = (w : workstation, set : mode, o : operation, s : simulation) : simulation =>
  /* get the operation's time */
  operationTimeOfMode(set, o.id)
  |> ( (time) => {
          let new_sim = substWorkstationStatus(w, Operation(time, time, o.id), s);
          Some(new_sim);
       })
  |=> s;

let areProductsInBuffer = (s : simulation, id : bufferId, arr : array(productQty)) : bool => {

  switch ( bufferOfSimulation(s, id) ) {
    | None => false
    | Some(buffer) => {
        let rec loop = (lst : list(productQty)) : bool =>
          switch (lst) {
            | [] => true
            | [pb, ...rest] => {
                let (pid, required) = pb;
                let available = howManyProducts(pid, buffer.products);
                if ( required > available ) {
                  false;
                } else {
                  loop(rest);
                };
              }
          }
        let lst = Array.to_list(arr);
        loop(lst);
      }
  };
};

let actionReady = (s : simulation, a : action) : bool =>
  switch (a.info) {
    | OnlyAssets(info) =>
        enoughAssets(s, info.asset_reqs)

    | ProductsAndAssets(info) => {
        let c1 = enoughAssets(s, info.asset_reqs);
        let c2 = areProductsInBuffer(s, info.buffer_id, info.product_reqs);

        c1 && c2;
      }
  };

let transportReady = (s : simulation, c : connection, p : productId) : bool => {
  let minOneProduct = (s : simulation, c : connection, p : productId) : bool =>
    areProductsInBuffer(s, c.from_buffer, [| (p,1) |]);

  switch ( c.constraints ) {
    | None => minOneProduct(s, c, p)
    | Some(products) => {
        let p_ok = Js.Array.some( (x) => x == p, products );
        if ( p_ok ) {
          minOneProduct(s, c, p);
        } else {
          false;
        };
      }
  };
};

let readyOperation = (s : simulation, id : operationId, b : buffer) : bool =>
  operationOfSimulation(s, id)
  |> ( (o) => {
          let (prods, assets) =
            Js.Array.reduce(
              (acc, pa) =>
                switch (pa) {
                  | (Product(pid), qty) => {
                      let (prods, assets) = acc;
                      let new_prods = Js.Array.concat( prods, [| (pid, qty) |] );
                      ( new_prods, assets );
                    }

                  | (Asset(aid), qty) => {
                      let (prods, assets) = acc;
                      let new_assets = Js.Array.concat( assets, [| (aid, qty) |] );
                      ( prods, new_assets );
                    }
                },
              ([||], [||]),
              o.info.reqs
            );
          let c1 = areProductsInBuffer(s, b.id, prods);
          let c2 = enoughAssets(s, assets);
          Some( c1 && c2 );
        } )
  |=> false;

let checkSimResources = (s : simulation, id : resourceId, qty : int) : option(bool) =>
  if ( qty < 0 ) {
    None;
  } else {
    resourceOfSimulation(s, id)
    |> ( (r) => {
           if ( r.available < qty ) {
             None;
           } else {
             Some(true);
           };
         } )
  };

let changeResource = (f : (int => int), res_id : resourceId, s : simulation) : simulation =>
  resourceOfSimulation(s, res_id)
  |> ( (r) => {
         let new_available = f(r.available);
         let new_resource = { ...r, available: new_available };
         let new_resources =
           Js.Array.map(
             (res : resource) =>
               if ( res.id == res_id ) {
                 new_resource;
               } else {
                 res;
               },
             s.scenario.resources
           );
         let new_scenario = { ...s.scenario, resources: new_resources };

         Some( { ...s, scenario: new_scenario } );
       } )
  |=> s;

let takeFromResources = (rid : resourceId, qty : int, s : simulation) : simulation =>
  changeResource( ((-)(qty)), rid, s );

let addToResources = (rid : resourceId, qty : int, s : simulation) : simulation =>
  changeResource( ((+)(qty)), rid, s );

let areResOkForWorkstationMode = (s : simulation, mid : modeId, w : workstation) : bool =>
  /* get the machine */
  machineOfSimulation(s, w.machine_id)

  /* get the info of all the needed resources */
  |> ( (m) =>
         modeOfMachine(m, mid)

         |> ( (mm) => {
                /* sum up all the required resources */
                let m_resources = m.resources_info;
                let mm_resources = mm.resources_info;
                let all_resources = addIdQty(m_resources, mm_resources);

                /* subtract them from the resources available on the workstation */
                let available_resources = w.resources;
                let diff = subIdQty(available_resources, all_resources);

                if ( isNegativeIdQty(diff) ) {
                  Some(false);
                } else {
                  Some(true);
                }
              } )
     )

  /* if something goes wrong, the answer is no */
  |=> false;

let changeWorkstationRes = (w : workstation, rid : resourceId, qty_change : int, s : simulation) : simulation => {
  let change = (rid, qty_change);
  let new_resources = addIdQty(w.resources, [|change|]);
  let new_workstation = { ...w, resources: new_resources };
  substWorkstation(new_workstation, s);
};

let assignResToWorkstation = (w : workstation, rid : resourceId, qty : int, s : simulation) : simulation =>
  changeWorkstationRes(w, rid, qty, s);

let releaseResFromWorkstation = (w : workstation, rid : resourceId, qty : int, s : simulation) : simulation =>
  changeWorkstationRes(w, rid, -qty, s);

let updateWorkstationMode = (wid : workstationId, s : simulation) : simulation =>
  workstationOfSimulation(s, wid)
  |> ( (w) =>
         switch (w.machine_mode) {
           | None => Some(s)
           | Some(mid) =>
               if ( areResOkForWorkstationMode(s, mid, w) ) {
                 Some(s);
               } else {
                 let new_workstation = { ...w, status: NotReady, machine_mode: None };
                 Some( substWorkstation(new_workstation, s) );
               }
         } )
  |=> s;

let checkWorkstationResources = (rid : resourceId, qty : int, w : workstation) : option(workstation) => {
  let tmp_res = [| (rid, qty) |];
  let diff_res = subIdQty(w.resources, tmp_res);
  if ( isNegativeIdQty(diff_res) ) {
    None;
  } else {
    Some(w);
  }
};

let checkWorkstationValidMode = (s : simulation, mid : modeId, w : workstation) : option(workstation) =>
  /* get the machine */
  machineOfSimulation(s, w.machine_id)

  /* get the machine mode */
  |> ( (m) => modeOfMachine(m, mid) )

  /* everything is ok */
  |> ( (_) => Some(w) );

let checkWorkstationResourcesForMode = (s : simulation, mid : modeId, w : workstation) : option(workstation) =>
  if ( areResOkForWorkstationMode(s, mid, w) ) {
    Some(w);
  } else {
    None;
  };

let startWorkstationMode = (s : simulation, mid : modeId, w : workstation) : option( (bool, simulation) ) =>
  /* get the machine */
  machineOfSimulation(s, w.machine_id)

  /* get the machine mode */
  |> ( (m) => modeOfMachine(m, mid) )

  /* update the workstation */
  |> ( (mm) => {
         let new_status = Setup(mm.time, mm.time, mid);
         let new_workstation = { ...w, status: new_status, machine_mode: None };
         let new_sim = substWorkstation(new_workstation, s);

         Some( (true, new_sim) );
       } );

let tickWorkstations = (s : simulation) : option(simulation) => {
  let new_sim =
    Js.Array.reduce(
      (acc, w : workstation) =>
        switch (w.status) {
          | Setup(time, total_time, mode_id) =>
              if ( time == 1 ) {
                /* finish the setup process */
                let new_status = Idle;
                let new_workstation = { ...w, status: new_status, machine_mode: Some(mode_id) };
                substWorkstation(new_workstation, acc);
              
              } else {
                /* update the timer */
                let new_status = Setup(time-1, total_time, mode_id);
                let new_workstation = { ...w, status: new_status };
                substWorkstation(new_workstation, acc);
              }

          | Operation(time, total_time, operation_id) =>
              if ( time == 1 ) {
                /* finish the operation */
                let new_workstation = { ...w, status: Idle };
                
                operationOfSimulation(acc, operation_id)
                
                |> ( (o) => {
                       let (asset_results, product_results) = operationResults(o);

                       let result =
                         acc
                         $> addToAssets(asset_results)
                         $> addToBuffer(w.buffer_id, product_results)
                         $> substWorkstation(new_workstation)
                         $=> true;

                       switch (result) {
                         | (true, new_acc) => Some(new_acc)
                         | (false, _) => Some(acc)
                       };
                     } )
                
                |=> acc;

              } else {
                /* update the timer */
                let new_workstation = { ...w, status: Operation(time-1, total_time, operation_id) };
                substWorkstation(new_workstation, acc);
              }

          | _ => acc
        },
      s,
      s.scenario.workstations
    );

  Some(new_sim);
};

let simRemoveHandler = (hid : handlerId, s : simulation) : simulation => {
  let new_handlers =
    Js.Array.reduce(
      (acc, h : handler) => {
        let (id, _) = h;
        if ( id == hid ) {
          acc;
        } else {
          Js.Array.concat( acc, [| h |] );
        }
      },
      [||],
      s.handlers
    );
  { ...s, handlers: new_handlers };
};

let simAddHandler = (s : simulation, h : handlerFunc) : (handlerId, simulation) => {
  let new_handlers = Js.Array.concat( s.handlers, [| (s.next_id, h) |] );
  let new_sim = { ...s, handlers: new_handlers, next_id: s.next_id + 1 };
  (s.next_id, new_sim);
};

