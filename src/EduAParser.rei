open EduATypes;
open EduAParserTypes;

/* Parse the passed lines, and return a scenario (when parsing is
   successful). */
let parse : array(string) => option(scenario);

/* Parse the passed lines, and return an array of parsed tables. */
let parseTables : array(string) => array(table);

