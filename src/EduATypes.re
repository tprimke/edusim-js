type okErr('a, 'b) =
  | Ok('a)
  | Err('b);

type idQty = (int, int);

/* All ids (they should be distinct, but we got what we can). */

type productId = int;
type actionId = int;
type operationId = int;
type modeId = int;
type machineId = int;
type resourceId = int;
type bufferId = int;
type workstationId = int;
type assetId = int;
type connectionId = int;
type handlerId = int;

type time = int;

type product = {
  id : productId,
  name : string
};

type prodOrAsset =
  | Product(productId)
  | Asset(assetId);

type reqOrRes = (prodOrAsset, int);

type operationInfo = {
  reqs : array(reqOrRes),
  results : array(reqOrRes)
};

type assetQty = (assetId, int);
type productQty = (productId, int);
type resourceQty = (resourceId, int);

type onlyAssets = {
  asset_reqs : array(assetQty),
  asset_results : array(assetQty)
};

type productsAndAssets = {
  buffer_id : bufferId,
  product_reqs : array(productQty),
  product_results : array(productQty),
  asset_reqs : array(assetQty),
  asset_results : array(assetQty)
};

type actionInfo =
  | OnlyAssets(onlyAssets)
  | ProductsAndAssets(productsAndAssets);

type action = {
  id : actionId,
  name : string,
  info : actionInfo
};

type operation = {
  id : operationId,
  name : string,
  info : operationInfo
};

type operationIdTime = (operationId, time);

type mode = {
  id : modeId,
  name : string,
  info : array(operationIdTime)
};

type resourceInfo = (resourceId, int);

type machineMode = {
  mode_id : modeId,
  time : time,
  resources_info : array(resourceInfo)
};

type machine = {
  id : int,
  name : string,
  mode_info : array(machineMode),
  resources_info : array(resourceInfo)
};

type resource = {
  id : resourceId,
  name : string,
  available : int,
  operation_time : time
};

type visInfo = {
  x : int,
  y : int
};

type buffer = {
  id : bufferId,
  name : string,
  products : array(productQty),
  vis : option(visInfo)
};

type workstationStatus =
  | NotReady
  | Setup(time, time, modeId) /* time to completion, total time */
  | Idle
  | Operation(time, time, operationId);

type workstation = {
  id : workstationId,
  name : string,
  resources : array(resourceQty),
  buffer_id : bufferId,
  machine_id : machineId,
  machine_mode : option(modeId),
  status : workstationStatus,
  vis : option(visInfo)
};

type connection = {
  id : connectionId,
  from_buffer : bufferId,
  to_buffer : bufferId,
  constraints : option( array(productId) )
};

type assetType = {
  id : int,
  name : string,
  initial : int
};

[@bs.deriving abstract]
type visConnection = {
  [@bs.as "type"] type_ : string,
  id : int
};

[@bs.deriving abstract]
type jsVisInfo = {
  [@bs.as "type"] type_ : string,
  id : int,
  x : int,
  y : int,
  inputs : array(visConnection),
  outputs: array(visConnection)
};

[@bs.deriving abstract]
type pbInfo = {
  product_id : productId,
  qty : int
};

[@bs.deriving abstract]
type bInfo = {
  id : bufferId,
  name : string,
  products : array(pbInfo)
};

[@bs.deriving abstract]
type aInfo = {
  id : assetId,
  name : string,
  value : int
};

type handlerBeforeBufferId = array(aInfo) => option(bInfo) => int => bool;
type handlerBefore2BuffersId = array(aInfo) => array(bInfo) => int => bool;
type handlerTick = array(aInfo) => array(bInfo) => int => unit;

type handlerFunc =
  | BeforeAction(actionId, handlerBeforeBufferId)
  | BeforeOperation(operationId, handlerBeforeBufferId)
  | BeforeTransfer(connectionId, handlerBefore2BuffersId)
  | BeforeTick(handlerTick);

type handler = (handlerId, handlerFunc);

type scenario = {
  products : array(product),
  assets : array(assetType),
  actions : array(action),
  operations : array(operation),
  modes : array(mode),
  resources : array(resource),
  machines : array(machine),
  buffers : array(buffer),
  workstations : array(workstation),
  connections : array(connection)
};

type simulation = {
  scenario : scenario,
  assets : array(assetQty),
  handlers : array(handler),
  next_id : handlerId,
  time : time
};


/*
  -------------------------------------------------------------------
  -- Types for JS information
  -------------------------------------------------------------------
*/

[@bs.deriving abstract]
type rInfo = {
  id : resourceId,
  name : string,
  available : int,
  operation_time : time
};

[@bs.deriving abstract]
type rSimpleInfo = {
  resource_id : resourceId,
  qty : int
};

[@bs.deriving abstract]
type wInfo = {
  id : workstationId,
  name : string,
  resources : array(rSimpleInfo),
  buffer_id : bufferId,
  machine_mode : Js.Nullable.t(int),
  status : string,
  [@bs.optional] time : int,
  [@bs.optional] totalTime : int,
  [@bs.optional] mode_id : int,
  [@bs.optional] operation_id : int
};

[@bs.deriving abstract]
type simulationInfo = {
  resources : array(rInfo),
  buffers : array(bInfo),
  assets : array(aInfo),
  workstations : array(wInfo)
};

[@bs.deriving abstract]
type actionReqResInfo = {
  pa : string, /* "product" | "asset" */
  id : int,
  qty : int
};

[@bs.deriving abstract]
type jsActionInfo = {
  id : int,
  name : string,
  requirements : array(actionReqResInfo),
  results : array(actionReqResInfo),
  [@bs.optional] buffer_id : int
};

[@bs.deriving abstract]
type jsTransportInfo = {
  id : int,
  buffer_from : bufferId,
  buffer_to : bufferId,
  [@bs.optional] products : array(productId)
};

[@bs.deriving abstract]
type jsReqOrResInfo = {
  id : int,
  [@bs.as "type"] type_ : string,
  qty : int
};

[@bs.deriving abstract]
type jsOperationInfo = {
  id : operationId,
  time : time,
  requirements : array(jsReqOrResInfo),
  results : array(jsReqOrResInfo)
};

[@bs.deriving abstract]
type jsResourceInfo = {
  id : int,
  qty : int
};

[@bs.deriving abstract]
type jsModeOperationsInfo = {
  id : modeId,
  time : time,
  resources : array(jsResourceInfo),
  operations : array(jsOperationInfo)
};

[@bs.deriving abstract]
type jsWorkstationInfo = {
  resources : array(jsResourceInfo),
  modes : array(jsModeOperationsInfo)
};

