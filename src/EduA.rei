open EduATypes;
open EduAParserTypes;

/* Parse the given lines into some scenario. */
let parse : array(string) => option(scenario);

/* Parse the passed lines, and return an array of parsed tables. */
let parseTables : array(string) => array(table);

/* Creates a new simulation object. */
let newSim : scenario => simulation;

/* Returns a JS object with all data regarding visualization of buffers
   and workstations*/
let visInfo : simulation => array(jsVisInfo);

/* Returns a JS object with all data regarding the simulation state. */
let info : simulation => simulationInfo;

/* Returns a JS object with all data regarding the simulation actions. */
let allActions : simulation => array(jsActionInfo);

/* Returns a JS array of all actions id, which can be started. */
let readyActions : simulation => array(actionId);

/* Returns a JS object with all data regarding the simulation transports. */
let allTransports : simulation => array(jsTransportInfo);

/* Returns a JS object with all data regarding the simulation transports,
   which can be started.. */
let readyTransports : simulation => array(jsTransportInfo);

/* Returns a JS object with all info regarding the requested workstation. */
let workstationInfo : (simulation, workstationId) => jsWorkstationInfo;

/* Returns a JS object with all data regarding the ready modes for the
  requested workstation. */
let readyModes : (simulation, workstationId) => array(modeId);

/* Returns a JS object with all data regarding the ready operations for
   the requested workstation. */
let readyOperations : (simulation, workstationId) => array(operationId);

/* Tries to execute the action. Returns (true, new_simulation) on success,
   and (false, simulation) otherwise. */
let execAction : (simulation, actionId) => (bool, simulation);

/* Assigns the requested resource, to the requested workstation. */
let assignResource : (simulation, workstationId, resourceId, int) => (bool, simulation);

/* Releases the requested resource, from the requested workstation. */
let releaseResource : (simulation, workstationId, resourceId, int) => (bool, simulation);

/* Tries to transfer products between connected buffers. */
let transfer : (simulation, connectionId, productId, int) => (bool, simulation);

/* Tries to start the operation, on the requested workstation.
   On success, returns (true, new simulation).
   On failure, returns (false, simulation). */
let startOperation : (simulation, workstationId, operationId) => (bool, simulation);

/* Tries to start setup of the given mode. */
let startMode : (simulation, workstationId, modeId) => (bool, simulation);

/* Calculates the next simulation state, after 1 time unit is passed. */
let tick : simulation => simulation;

/* Tries to add a new handler, that should be run before the action
   is executed. Returns (true, handler_id, new_simulation) on success. */
let addBeforeAction : (simulation, actionId, handlerBeforeBufferId) => (bool, handlerId, simulation);

/* Removed the requested handler. Returns (true, new_simulation)
   on success. */
let removeHandler : (simulation, handlerId) => (bool, simulation);

