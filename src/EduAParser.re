open EduATypes;
open EduAUtils;
open EduAParserTypes;

let intOfStr = (s : string) : int => Int32.to_int( Int32.of_string(s) );

let cellValueOfStr = (line_num : int, s : string, t : cellType) : cellValue =>
  switch (t, s) {
    | (_, "null") => Null
    | (CStr, _) => Str(s)
    | (CInt, _) =>
        try ( Int( intOfStr(s) ) ) {
          | _ => {
              let str_line = Js.Int.toString(line_num);
              let err_msg = "Line " ++ str_line ++ ": the value " ++ s ++ " is not an int.";
              raise(ParseException([| err_msg |]));
            }
        }
    | (CEnum(values), _) =>
        if ( Js.Array.indexOf(s, values) > -1 ) {
          Str(s)
        } else {
           let str_line = Js.Int.toString(line_num);
           let err_msg = "Line " ++ str_line ++ ": the value " ++ s ++ " is incorrect.";
           raise(ParseException([| err_msg |]));
        }
    | _ => NotAValue(line_num)
  };

let rowOfCells = (info : (int, cells), types : array(cellType)) : tableRow => {
  let (line_num, cells) = info;
  let cells_num = Js.Array.length(cells);
  let types_num = Js.Array.length(types);
  if ( cells_num == types_num ) {
    let cell_values = Js.Array.mapi(
      (s, i) => {
        let t = types[i];
        cellValueOfStr(line_num, s, t);
      },
      cells
    );
    (line_num, cell_values);
  } else {
    let str_line = Js.Int.toString(line_num);
    let err_msg = "Line " ++ str_line ++ ": incorrect number of columns.";
    raise(ParseException([|err_msg|]));
  };
};

let rowsOfAllCells = (cells : array((int, cells)), types : array(cellType)) : array(tableRow) =>
  Js.Array.map( (cells) => rowOfCells(cells, types), cells );

let getTable = (tables : array(table), name : tableName) : table => {
  let check_name = (t : table, n : string) : bool => {
    let (t_name, _) = t;
    t_name == n;
  };
  let result = Js.Array.filter( (x) => check_name(x, name), tables );
  result[0];
};

let getTableRows = (tables : array(table), name : tableName, idx : int, value : cellValue) : array(tableRow) => {
  let (_, rows) = getTable(tables, name);
  let check_row = (row, idx, value) => {
    let (_, row_values) = row;
    let cell_value = row_values[idx];
    cell_value == value;
  };
  Js.Array.filter( (x) => check_row(x, idx, value), rows );
};


/* ------------------------------------------------------------------ */
/* Functions to transform tables into data structures. */

/* Auxiliary function (a constructor). */
let parseError = (line_num : lineNumber, msg : string) : parsedRow('a) => {
  let str_line = Js.Int.toString(line_num);
  ParseError("Line " ++ str_line ++ ": " ++ msg);
};

let splitRows = (rows : array(parsedRow('a))) : (array('a), array(string)) =>
  Js.Array.reduce(
    ( (aas, errs), p_row ) =>
      switch (p_row) {
        | Row(r) => {
            ignore( Js.Array.push(r, aas) );
            (aas, errs);
          }

        | ParseError(e) => {
            ignore( Js.Array.push(e, errs) );
            (aas, errs);
          }
      },
    ([||], [||]),
    rows
  );

let scenarioProducts = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Products");
  let product_of_row = ((line_num, row)) : parsedRow(product) =>
    switch (row) {
      | [| Int(product_id), Str(name) |] =>
          Row( { id: product_id, name: name } )

      | _ => parseError(line_num, "incorrect format")
    };
  let parsed_rows = Js.Array.map( product_of_row, rows );
  let (products, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pproducts: Some(products)};
  (new_scen, errs);
};

/* let empty = (a : array('a)) : bool => Js.Array.length(a) == 0; */
let notEmpty = (a : array('a)) : bool => Js.Array.length(a) > 0;

let scenarioActions = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Actions");
  let action_of_row = ((line_num, row)) : parsedRow(action) =>
    switch (row) {
      | [| Int(action_id), Str(name), Int(buffer_id) |] => {
          let info_rows = getTableRows(tables, "ActionsReqsResults", 0, Int(action_id));
          let req_products = [||];
          let req_assets = [||];
          let res_products = [||];
          let res_assets = [||];
          Js.Array.forEach(
            ((_line_num, r)) => switch (r) {
              | [| _, Str(rr), Str(ap), Int(ref_id), Int(qty) |] => {
                  let value = (ref_id, qty);
                  let arr = switch (rr, ap) {
                    | ("req", "asset") => req_assets
                    | ("res", "asset") => res_assets
                    | ("req", "product") => req_products
                    | ("res", "product") => res_products
                    | _ => req_assets /* should NEVER happen */
                  };
                  ignore( Js.Array.push( value, arr ) );
                  ();
                }

              | _ => ()
            },
            info_rows
          );
          
          let some_req_assets = notEmpty(req_assets);
          let some_res_assets = notEmpty(res_assets);
          let some_req_products = notEmpty(req_products);
          let some_res_products = notEmpty(res_products);
          
          switch (some_req_assets, some_res_assets, some_req_products, some_res_products) {
            
            | (true, true, false, false) => {
                /* only assets */
                let info = {
                  asset_reqs: req_assets,
                  asset_results: res_assets
                };

                Row( { id: action_id, name: name, info: OnlyAssets(info) } );
              }

            | _ => {
                /* buffer with products and assets */
                let info = {
                  buffer_id: buffer_id,
                  product_reqs : req_products,
                  product_results : res_products,
                  asset_reqs: req_assets,
                  asset_results: res_assets
                };

                Row( { id: action_id, name: name, info: ProductsAndAssets(info) } );
              }

          }
        }

      | _ => parseError(line_num, "incorrect format")

  };
  let parsed_rows = Js.Array.map( action_of_row, rows );
  let (actions, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pactions: Some(actions)};
  (new_scen, errs);
};

let scenarioOperations = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Operations");
  let operation_of_row = ((line_num, row)) : parsedRow(operation) =>
    switch (row) {
      | [| Int(operation_id), Str(name) |] => {
          let info_rows = getTableRows(tables, "OperationsReqsResults", 0, Int(operation_id));
          let reqs = [||];
          let results = [||];
          Js.Array.forEach(
            ((_line_num, r)) => switch (r) {
              | [| _, Str(rr), Str(str_ap), Int(ref_id), Int(qty) |] => {
                  let ap = switch (str_ap) {
                    | "asset" => (Asset(ref_id), qty)
                    | "product" => (Product(ref_id), qty)
                    | _ => (Asset(ref_id), qty)
                  };
                  let arr = switch (rr) {
                    | "req" => reqs
                    | "res" => results
                    | _ => reqs
                  };
                  ignore( Js.Array.push( ap, arr ) );
                  ();
                }

              | _ => ()
            },
            info_rows
          );
          let info = { reqs: reqs, results: results };
          Row( { id: operation_id, name: name, info: info } );
        }

      | _ => parseError(line_num, "incorrect format")
  };
  let parsed_rows = Js.Array.map( operation_of_row, rows );
  let (operations, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, poperations: Some(operations)};
  (new_scen, errs);
};

let scenarioAssets = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Assets");
  let asset_of_row = ((line_num, row)) : parsedRow(assetType) =>
    switch (row) {
      | [| Int(asset_id), Str(name), Int(initial_value) |] =>
          Row( { id: asset_id, name: name, initial: initial_value } )

      | _ =>
          parseError(line_num, "incorrect format")
  };
  let parsed_rows = Js.Array.map( asset_of_row, rows );
  let (assets, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, passets: Some(assets)};
  (new_scen, errs);
};

let scenarioModes = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Modes");
  let set_of_row = ((line_num, row)) : parsedRow(mode) =>
    switch (row) {
      | [| Int(mode_id), Str(name) |] => {
          let info_rows = getTableRows(tables, "ModeOperations", 0, Int(mode_id));
          let infos = [||];
          Js.Array.forEach(
            ((_line_num, r)) => switch (r) {
              | [| _, Int(operation_id), Int(time) |] => {
                  let op_time = (operation_id, time);
                  ignore( Js.Array.push(op_time, infos) );
                  ();
                }

              | _ => ()
            },
            info_rows
          );
          Row( { id: mode_id, name: name, info: infos } );
        }

      | _ => parseError(line_num, "incorrect format")
  };
  let parsed_rows = Js.Array.map( set_of_row, rows );
  let (modes, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pmodes: Some(modes)};
  (new_scen, errs);
};

let scenarioResources = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Resources");
  let resource_of_row = ((line_num, row)) : parsedRow(resource) =>
    switch (row) {
      | [| Int(resource_id), Str(name), Int(available), Int(op_time) |] =>
          Row( { id: resource_id, name: name, available: available, operation_time: op_time } )

      | _ =>
          parseError(line_num, "incorrect format")
  };
  let parsed_rows = Js.Array.map( resource_of_row, rows );
  let (resources, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, presources: Some(resources)};
  (new_scen, errs);
};

let scenarioMachines = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Machines");
  let machine_of_row = ((line_num, row)) : parsedRow(machine) =>
    switch (row) {
      | [| Int(machine_id), Str(name) |] => {
          let modes = getTableRows(tables, "MachineModes", 0, Int(machine_id));
          let resources = getTableRows(tables, "MachineResources", 0, Int(machine_id));
          let sets = [||];
          let ress = [||];
          Js.Array.forEach(
            ((_line_num, r)) => switch (r) {
              | [| _, Int(resource_id), Int(required) |] => {
                  let res_info = (resource_id, required);
                  ignore( Js.Array.push(res_info, ress) );
                  ();
                }

              | _ => ()
            },
            resources
          );
          Js.Array.forEach(
            ((_line_num, r)) =>
              switch (r) {
                | [| _, Int(mode_id), Int(time) |] => {
                    let ress = getTableRows(tables, "MachineModeResources", 0, Int(machine_id));
                    let infos = [||];
                    Js.Array.forEach(
                      ((_line_num, r)) =>
                        switch (r) {
                          | [| _, _, Int(resource_id), Int(required) |] => {
                              let info = (resource_id, required);
                              ignore( Js.Array.push(info, infos) );
                              ();
                            }
            
                          | _ => ()
                        },
                      ress
                    );
                    let info = { mode_id: mode_id, time: time, resources_info: infos };
                    ignore( Js.Array.push( info, sets ) );
                    ();
                  }

                | _ => ()
            },
            modes
          );
          Row( { id: machine_id, name: name, mode_info: sets, resources_info: ress } );
        }

      | _ => parseError(line_num, "incorrect format")
  };
  let parsed_rows = Js.Array.map( machine_of_row, rows );
  let (machines, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pmachines: Some(machines)};
  (new_scen, errs);
};

let scenarioBuffers = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Buffers");
  let buffer_of_row = ((line_num, row)) : parsedRow(buffer) =>
    switch (row) {
      | [| Int(buffer_id), Str(name) |] => {
          let info_rows = getTableRows(tables, "BufferProducts", 0, Int(buffer_id));
          let products = [||];
          Js.Array.forEach(
            ((_line_num, r)) =>
              switch (r) {
                | [| _, Int(product_id), Int(qty) |] => {
                    let prod_buf = (product_id, qty);
                    ignore( Js.Array.push(prod_buf, products) );
                    ();
                  }

                | _ => ()
              },
            info_rows
          );
          Row( { id: buffer_id, name: name, products: products, vis: None } );
        }

      | _ => parseError(line_num, "incorrect format")
    };
  let parsed_rows = Js.Array.map( buffer_of_row, rows );
  let (buffers, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pbuffers: Some(buffers)};
  (new_scen, errs);
};

let scenarioWorkstations = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Workstations");
  let workstation_of_row = ((line_num, row)) : parsedRow(workstation) =>
    switch (row) {
      | [| Int(workstation_id), Str(name), Int(buffer_id), Int(machine_id), m_mode, m_target_id, Str(str_status), m_timer |] =>
          switch (str_status) {
            | "not ready" => {
                /* no mode, nothing in progress */
                Row( { id: workstation_id,
                  name: name,
                  resources: [||],
                  buffer_id: buffer_id,
                  machine_id: machine_id,
                  machine_mode: None,
                  vis: None,
                  status: NotReady } )
              }

            | "setup" =>
                switch ( (m_target_id, m_timer) ) {
                  | ( Int(target_id), Int(timer) ) =>
                      Row( { id: workstation_id,
                        name: name,
                        resources: [||],
                        buffer_id: buffer_id,
                        machine_id: machine_id,
                        machine_mode: None,
                        vis: None,
                        status: Setup(timer, timer, target_id) } )
                  | _ => parseError(line_num, "incorrect setup")
                }

            | "idle" =>
                switch (m_mode) {
                  | Int(mode) =>
                      Row( { id: workstation_id,
                        name: name,
                        resources: [||],
                        buffer_id: buffer_id,
                        machine_id: machine_id,
                        machine_mode: Some(mode),
                        vis: None,
                        status: Idle } )

                  | _ => parseError(line_num, "incorrect idle state")
                }

            | "operation" =>
                switch (m_target_id, m_timer, m_mode) {
                  | ( Int(target_id), Int(timer), Int(mode) ) =>
                      Row( { id: workstation_id,
                        name: name,
                        resources: [||],
                        buffer_id: buffer_id,
                        machine_id: machine_id,
                        machine_mode: Some(mode),
                        vis: None,
                        status: Operation(timer, timer, target_id) } )

                  | _ => parseError(line_num, "incorrect operation")
                }

            | _ => /* should NEVER happen */
                parseError(line_num, "incorrect format")

          }

      | _ => /* should NEVER happen */
          parseError(line_num, "incorrect format")
    };
  let parsed_rows = Js.Array.map( workstation_of_row, rows );
  let (workstations, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pworkstations: Some(workstations)};
  (new_scen, errs);
};

let scenarioConnections = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Connections");
  let connection_of_row = ((line_num, row)) : parsedRow(connection) => {
    
    let unpack = (r : array(cellValue)) : option( (int, int, int) ) =>
      switch (r) {
        | [| Int(connection_id), Int(from_buffer), Int(to_buffer) |] => {
            let t = (connection_id, from_buffer, to_buffer);
            Some(t);
          }

        | _ => None
      };

    let process = (tpl) : option(parsedRow(connection)) => {
      let (connection_id, from_buffer, to_buffer) = tpl;
      let c_rows = getTableRows(tables, "ConnectionConstraints", 0, Int(connection_id));
      let products = [||];
      Js.Array.forEach(
        ((_line_num, r)) => {
          switch (r) {
            | [| _, Int(product_id) |] =>
                ignore( Js.Array.push(product_id, products) )
            | _ => 
                ()
          };
        },
        c_rows
      );
      let constraints = switch (products) {
        | [||] => None
        | _ => Some(products)
      };
      Some ( Row({ id: connection_id, from_buffer: from_buffer, to_buffer: to_buffer, constraints: constraints }) );
    };

    Some(row)
    |> unpack
    |> process
    |=> parseError(line_num, "incorrect format");

  };
  let parsed_rows = Js.Array.map( connection_of_row, rows );
  let (connections, errs) = splitRows(parsed_rows);
  let new_scen = {...scen, pconnections: Some(connections)};
  (new_scen, errs);
};

let scenarioVisualization = (tables : array(table), scen : parsedScenario) : parseState => {
  let (_, rows) = getTable(tables, "Visualization");
  
  let pos = (a_type : string, id : int) : option((int, int)) =>
    Js.Array.reduce(
      (acc, (_, row)) =>
        switch (acc) {
          | None => {
              let [| Str(row_type), Int(row_id), Int(x), Int(y) |] = row;
              if ( (a_type == row_type) && (id == row_id) ) {
                Some( (x, y) )
              } else {
                None
              };
            }
          | v_else => v_else
        },
      None,
      rows
    );

  let processBuffer = (b : buffer) : buffer =>
    switch ( pos("buf", b.id) ) {
      | None => b
      | Some((x, y)) =>
          {...b, vis: Some({x, y})}
    };

  let processWorkstation = (w : workstation) : workstation =>
    switch ( pos("wst", w.id) ) {
      | None => w
      | Some((x, y)) =>
          {...w, vis: Some({x, y})}
    };
  
  /* process the visualization rows, and update the buffers and workstations */
  switch ( (scen.pbuffers, scen.pworkstations) ) {
    | ( (Some(bufs), Some(wsts)) ) => {
          let new_buffers = Js.Array.map( processBuffer, bufs );
          let new_workstations = Js.Array.map( processWorkstation, wsts );
          let new_scen = {...scen, pbuffers: Some(new_buffers), pworkstations: Some(new_workstations)};
          (new_scen, [||]);
        }

    /* actually, it's some enormous situation, and should NEVER happen
       both buffers and workstations should've been parsed before */
    | _ => (scen, [||])
  };
};


/* ------------------------------------------------------------------ */
/* Parser functions. */

let tokens = (line, sep : string) : array(string) => {
  let trimmed = Js.String.trim(line);
  let tkns = Js.String.split(sep, trimmed);
  Js.Array.map( (x) => Js.String.trim(x), tkns );
};

let ctReqRes = CEnum([| "req", "res" |])
let ctAssetProduct = CEnum([| "asset", "product" |])

let toTable = (table_name : tableName, cells : array((int, cells))) : table => {
  switch (table_name) {
    | "Products" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr|]))

    | "Actions" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr, CInt|]))

    | "ActionsReqsResults" =>
        (table_name, rowsOfAllCells(cells, [|CInt, ctReqRes, ctAssetProduct, CInt, CInt|]))

    | "Operations" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr|]))

    | "OperationsReqsResults" =>
        (table_name, rowsOfAllCells(cells, [|CInt, ctReqRes, ctAssetProduct, CInt, CInt|]))

    | "Modes" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr|]))

    | "ModeOperations" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt|]))

    | "Machines" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr|]))

    | "MachineModes" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt|]))

    | "MachineResources" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt|]))

    | "MachineModeResources" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt, CInt|]))
      
    | "Resources" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr, CInt, CInt|]))
      
    | "Buffers" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr|]))
      
    | "BufferProducts" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt|]))
      
    | "Workstations" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr, CInt, CInt, CInt, CInt, CEnum([| "not ready", "setup", "idle", "operation" |]), CInt|]))
      
    | "Connections" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt, CInt|]))
      
    | "ConnectionConstraints" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CInt|]))
      
    | "Assets" =>
        (table_name, rowsOfAllCells(cells, [|CInt, CStr, CInt|]))

    | "Visualization" =>
        (table_name, rowsOfAllCells(cells, [|CEnum([| "buf", "wst" |]), CInt, CInt, CInt|]))
      
    | _ => {
        let err_msg = "Unexpected table: " ++ table_name;
        raise(ParseException([| err_msg |]));
      }
  }
};

let parseLine = (line : string, mode : parserMode) : lineType =>
  switch (mode) {
    | NeedTableLine => {
        if ( Js.String.startsWith("-- ", line) ) {
          let arr = tokens(line, " ");
          Table(arr[1]);
        } else {
          NotImportant
        }
      }

    | NeedDataLine(_) => {
        let arr = tokens(line, ",");
        switch (arr) {
          | [||] => NotImportant;
          | _ => Data(arr);
        };
      }
  };


/* ------------------------------------------------------------------ */
/* The main parse function. */

let parseTables = (lines : array(string)) : array(table) => {
  /* string lines to tables */

  let rec f = (num, lines, mode, tables) => {
      switch (lines) {
        | [] => tables
        | [line, ...rest] => {
            let trimmed = Js.String.trim(line);
            switch (trimmed, mode) {
              | ("", NeedTableLine) => f(num + 1, rest, NeedTableLine, tables)
              | ("", NeedDataLine((table_name, cells))) => {
                  let table = toTable(table_name, cells);
                  ignore( Js.Array.push(table, tables) );
                  f(num + 1, rest, NeedTableLine, tables);
                }
              | _ => {
                  let parsed = parseLine(trimmed, mode);
                  switch (parsed, mode) {
                    | (NotImportant, NeedTableLine) => f(num + 1, rest, NeedTableLine, tables)
                    | (NotImportant, NeedDataLine((table_name, cells))) => {
                        let table = toTable(table_name, cells);
                        ignore( Js.Array.push(table, tables) );
                        f(num + 1, rest, NeedTableLine, tables);
                      }
                    | (Table(name), NeedTableLine) => f(num + 1, rest, NeedDataLine((name, [||])), tables)
                    | (Data(cells), NeedDataLine((table_name, old_cells))) => {
                        let new_row_of_cells = (num, cells);
                        ignore(Js.Array.push(new_row_of_cells, old_cells));
                        f(num + 1, rest, NeedDataLine((table_name, old_cells)), tables);
                      }
                    | _ => /* impossible case - should NEVER happen */
                        f(num + 1, rest, NeedTableLine, tables)
                  }
                }
            }
          }
      }
  };
  let lst = Array.to_list(lines);
  f(1, lst, NeedTableLine, [||]);
};

let (%>) = (ps : parseState, f) =>
  switch (ps) {
    | (state, [||]) => {
        let (new_state, parsed_errs) = f(state);
        (new_state, parsed_errs);
      }

    | _ => ps
  };

let parse = (lines : array(string)) : option(scenario) => {
  /* string lines to tables */
  let tables = parseTables(lines);

  /* tables to scenario */
  let ps : parsedScenario = {
    pproducts: None,
    passets : None,
    pactions : None,
    poperations : None,
    pmodes : None,
    presources : None,
    pmachines : None,
    pbuffers : None,
    pworkstations : None,
    pconnections : None
  };

  let state =
    (ps, [||])
      %> scenarioProducts(tables)
      %> scenarioAssets(tables)
      %> scenarioActions(tables)
      %> scenarioOperations(tables)
      %> scenarioModes(tables)
      %> scenarioResources(tables)
      %> scenarioMachines(tables)
      %> scenarioBuffers(tables)
      %> scenarioWorkstations(tables)
      %> scenarioConnections(tables)
      %> scenarioVisualization(tables);

  switch (state) {
    | (s, [||]) => {
        let { pproducts, passets, pactions, poperations, pmodes, presources, pmachines, pbuffers, pworkstations, pconnections } = s;
        switch ( (pproducts, passets, pactions, poperations, pmodes, presources, pmachines, pbuffers, pworkstations, pconnections) ) {
          | (Some(pr), Some(ass), Some(ac), Some(op), Some(mo), Some(re), Some(ma), Some(bu), Some(wo), Some(co)) => {
              let scenario = {
                products: pr, assets: ass, actions: ac, operations: op,
                modes: mo, resources: re, machines: ma, buffers: bu,
                workstations: wo, connections: co
              };

              Some(scenario);
            }

          // in theory, it should NEVER happen
          | _ => None
        };
      }

    | (_, errs) =>
        raise(ParseException(errs))
  };

};


