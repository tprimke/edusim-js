var chai = require('chai');
var assert = chai.assert;

var edua = require('../lib/js/src/EduA.bs');

describe('EduA', () => {
  
  var spec, scen, sim;

  let testSpec = () => {
    let num_spec = spec.map( (line, idx) => [idx, line] );
    console.log(num_spec);
  };

  beforeEach( () => {
    var fs = require('fs');
    spec = fs.readFileSync('test/opt-challenge', 'utf8');
    spec = spec.split('\n');
  });

  describe("Parser", () => {

    describe("ActionReqsResults", () => {

      it("too many columns", () => {
        spec[24] += ", too_many";
        assert.throws( () => edua.parse(spec) );
      });

      it("too few columns", () => {
        spec[24] = "1, req, asset, 1";
        assert.throws( () => edua.parse(spec) );
      });

      it("incorrect type of id", () => {
        spec[24] = "one, req, asset, 1, 10";
        assert.throws( () => edua.parse(spec) );
      });

      it("incorrect value of req/res (a typo)", () => {
        spec[24] = "1, rek, asset, 1, 10";
        assert.throws( () => edua.parse(spec) );
      });

      it("incorrect value of asset/product (a typo)", () => {
        spec[24] = "1, req, ziutek, 1, 10";
        assert.throws( () => edua.parse(spec) );
      });

    });

  });

});

