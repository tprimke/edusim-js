var chai = require('chai');
var assert = chai.assert;

var edua = require('../lib/js/src/EduA.bs');

describe('OPT Challenge', () => {
  
  var spec, scen, sim;

  beforeEach( () => {
  	var fs = require('fs');
  	spec = fs.readFileSync('test/opt-challenge', 'utf8');
  	spec = spec.split('\n');
  	scen = edua.parse(spec);
  	sim = edua.newSim(scen);
  });

  let workstationOfSim = (sim, wst_id) => {
    let info = edua.info(sim);
    assert.property(info, "workstations");
    assert.isArray(info.workstations);
    let wst = info.workstations.filter( (w) => w.id === wst_id );
    assert.isArray(wst);
    assert.equal(wst.length, 1);

    return wst[0];
  };

  let bufferOfSim = (sim, b_id) => {
    let info = edua.info(sim);
    assert.property(info, "buffers");
    assert.isArray(info.buffers);
    let b = info.buffers.filter( (buf) => buf.id === b_id );
    assert.isArray(b);
    assert.equal(b.length, 1);
    
    return b[0];
  };

  let bufferOfWorkstation = (sim, wst) => {
    assert.property(wst, "buffer_id");
    let b_id = wst.buffer_id;
    
    return bufferOfSim(sim, b_id);
  };

  let checkWstInfoProp = (sim, wst_id, prop_name, prop_value) => {
    let wst = workstationOfSim(sim, wst_id);

    assert.property(wst, prop_name);
    if ( prop_value === null )
      assert.isNull(wst[prop_name]);
    else
      assert.equal(wst[prop_name], prop_value);
  };

  let chkProduct = (b, product_id, qty) => {
    assert.property(b, "products");
    assert.isArray(b.products);
    assert.deepInclude(b.products, { product_id, qty });
  };

  let chkNoProduct = (b, product_id) => {
    assert.property(b, "products");
    assert.isArray(b.products);
    let arr = b.products.filter( (x) => x.product_id === product_id );
    assert.equal(arr.length, 0);
  };

  let checkProductInBuffer = (sim, buf_id, product_id, qty) => {
    let b = bufferOfSim(sim, buf_id);
    chkProduct(b, product_id, qty);
  };

  let checkNoProductInBuffer = (sim, buf_id, product_id) => {
    let b = bufferOfSim(sim, buf_id);
    chkNoProduct(b, product_id);
  };

  let checkProductInWorkstationBuffer = (sim, wst_id, product_id, qty) => {
    let wst = workstationOfSim(sim, wst_id);
    let b = bufferOfWorkstation(sim, wst);

    // check the product
    chkProduct(b, product_id, qty);
  };

  let checkNoProductInWorkstationBuffer = (sim, wst_id, product_id) => {
    let wst = workstationOfSim(sim, wst_id);
    let b = bufferOfWorkstation(sim, wst);

    // check the product
    chkNoProduct(b, product_id);
  };

  it( "initial state", () => {
  	let info = edua.info(sim);

  	// Only tests essential for further tests.

  	let assets = info.assets;
  	assert.deepNestedPropertyVal(assets[0], 'name', 'money');
  	assert.deepNestedPropertyVal(assets[0], 'value', 1500);
  });

  describe('actions', () => {

  	it("Ready actions at the beginning", () => {
  	  let actions = edua.readyActions(sim);
  	  assert.include(actions, 1);
  	  assert.include(actions, 3);
  	  assert.lengthOf(actions, 2);
  	});

  	it("Buy the raw material A", () => {
  	  let [result, new_sim] = edua.execAction(sim, 1);
  	  assert.equal(result, true);
  	  let info = edua.info(new_sim);
  	  let assets = info.assets;
  	  let buffers = info.buffers;
  	  assert.deepNestedPropertyVal(assets[0], 'value', 1490);
  	  assert.deepNestedPropertyVal(buffers[0], 'id', 1);
  	  assert.deepInclude(buffers[0].products, { product_id: 1, qty: 1 });
  	});

  	it("Buy the raw material B", () => {
  	  let [result, new_sim] = edua.execAction(sim, 3);
  	  assert.equal(result, true);
  	  let info = edua.info(new_sim);
  	  let assets = info.assets;
  	  let buffers = info.buffers;
  	  assert.deepNestedPropertyVal(assets[0], 'value', 1490);
  	  assert.deepNestedPropertyVal(buffers[0], 'id', 1);
  	  assert.deepInclude(buffers[0].products, { product_id: 4, qty: 1 });
  	});

  });

  describe('resource management', () => {

    it("assign the resources and release them", () => {
      // at the beginning, all the resources should be available
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1); // wst_id, res_id, qty
      assert.equal(result, true);
      
      [result, new_sim] = edua.assignResource(new_sim, 1, 2, 1); // wst_id, res_id, qty
      assert.equal(result, true);

      [result, new_sim] = edua.assignResource(new_sim, 1, 3, 1); // wst_id, res_id, qty
      assert.equal(result, true);

      // then, release all the resources
      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 1); // wst_id, res_id, qty
      assert.equal(result, true);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 2, 1); // wst_id, res_id, qty
      assert.equal(result, true);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 3, 1); // wst_id, res_id, qty
      assert.equal(result, true);
    });

    it("cannot assign more than there is to assign", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 2); // wst_id, res_id, qty
      assert.equal(result, false);
      [result, new_sim] = edua.assignResource(new_sim, 1, 2, 2); // wst_id, res_id, qty
      assert.equal(result, false);
      [result, new_sim] = edua.assignResource(new_sim, 1, 3, 2); // wst_id, res_id, qty
      assert.equal(result, false);
    });

    it("cannot release more than was assigned", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1); // wst_id, res_id, qty
      [result, new_sim] = edua.releaseResource(new_sim, 2, 1, 1);
      assert.equal(result, false);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 2, 1);
      assert.equal(result, false);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 2);
      assert.equal(result, false);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 1);
      assert.equal(result, true);
    });

    it("the same resource cannot be assignbed to more than one workstation", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1); // wst_id, res_id, qty
      [result, new_sim] = edua.assignResource(new_sim, 2, 1, 1);
      assert.equal(result, false);
      [result, new_sim] = edua.assignResource(new_sim, 3, 1, 1);
      assert.equal(result, false);
      [result, new_sim] = edua.assignResource(new_sim, 4, 1, 1);
      assert.equal(result, false);
      [result, new_sim] = edua.assignResource(new_sim, 5, 1, 1);
      assert.equal(result, false);
    });

    it("assignment of resources", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      let info = edua.info(new_sim);
      let wst = info.workstations.filter( (w) => w.id === 1 )[0];
      assert.deepInclude(wst.resources, {resource_id: 1, qty: 1});
    });

    it("release of resources", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 1);
      assert.equal(result, true);
      let info = edua.info(new_sim);
      let wst = info.workstations.filter( (w) => w.id === 1 )[0];
      assert.notDeepInclude(wst.resources, {resource_id: 1, qty: 1});
      assert.equal(wst.resources.length, 0);
    });

  });

  describe("workstation mode management", () => {

    it("with all resources in sumilation, all modes should be ready for any workstation", () => {
      let result = edua.readyModes(sim, 1);
      assert.equal( result.length, 1 );
      assert.deepInclude( result, 1 );
      result = edua.readyModes(sim, 2);
      assert.equal( result.length, 1 );
      assert.deepInclude( result, 2 );
      result = edua.readyModes(sim, 3);
      assert.equal( result.length, 1 );
      assert.deepInclude( result, 3 );
    });

    it("with no resources in simulation, no mode should be ready for any workstation", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 2, 1);
      [result, new_sim] = edua.assignResource(new_sim, 2, 3, 1);
      [result, new_sim] = edua.assignResource(new_sim, 3, 1, 1);

      result = edua.readyModes(new_sim, 1);
      assert.equal( result.length, 0 );
      result = edua.readyModes(new_sim, 2);
      assert.equal( result.length, 0 );
      result = edua.readyModes(new_sim, 3);
      assert.equal( result.length, 0 );
    });

    it("with proper resources, proper modes should be ready for workstations", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      [result, new_sim] = edua.assignResource(sim, 2, 2, 1);
      [result, new_sim] = edua.assignResource(sim, 3, 3, 1);

      result = edua.readyModes(sim, 1);
      assert.deepInclude(result, 1);
      result = edua.readyModes(sim, 2);
      assert.deepInclude(result, 2);
      result = edua.readyModes(sim, 3);
      assert.deepInclude(result, 3);
    });

    it("for the workstation 1, change mode can be initiated", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      [result, new_sim] = edua.startMode(new_sim, 1, 1); // wst_id, mode_id
      assert.equal(result, true);

      checkWstInfoProp(new_sim, 1, "machine_mode", null);
      checkWstInfoProp(new_sim, 1, "mode_id", 1);
      checkWstInfoProp(new_sim, 1, "status", "setup");
      checkWstInfoProp(new_sim, 1, "time", 240);
      checkWstInfoProp(new_sim, 1, "totalTime", 240);
    });

    it("without needed resources, mode cannot be set", () => {
      let [result, _new_sim] = edua.startMode(sim, 1, 1);
      assert.equal(result, false);
    });

    it("after the set up time, the mode is ready", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      [result, new_sim] = edua.startMode(new_sim, 1, 1); // wst_id, mode_id
      for ( let i = 0; i < 239; i++ ) {
        new_sim = edua.tick(new_sim);
      }

      checkWstInfoProp(new_sim, 1, "machine_mode", null);
      checkWstInfoProp(new_sim, 1, "mode_id", 1);
      checkWstInfoProp(new_sim, 1, "status", "setup");
      checkWstInfoProp(new_sim, 1, "time", 1);
      checkWstInfoProp(new_sim, 1, "totalTime", 240);

      new_sim = edua.tick(new_sim);

      checkWstInfoProp(new_sim, 1, "machine_mode", 1);
      checkWstInfoProp(new_sim, 1, "status", "idle");

      // after the mode is set, it should be no longer ready
      result = edua.readyModes(new_sim, 1);
      assert.deepEqual(result, []);
    });

    it("when the mode is ready, releasing resource resets it", () => {
      let [result, new_sim] = edua.assignResource(sim, 1, 1, 1);
      [result, new_sim] = edua.startMode(new_sim, 1, 1); // wst_id, mode_id
      for ( let i = 0; i < 240; i++ ) {
        new_sim = edua.tick(new_sim);
      }

      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 1);
      assert.equal(result, true);

      checkWstInfoProp(new_sim, 1, "machine_mode", null);
      checkWstInfoProp(new_sim, 1, "status", "not ready");
    });
  });

  describe("transfer", () => {
    
    it("no transfer when no products to be transferred", () => {
      let [result, new_sim] = edua.transfer(sim, 1, 1, 1); // connection_id, product_id, qty
      assert.equal(result, false);
    });

    it("cannot transfer more than available", () => {
      let [result, new_sim] = edua.execAction(sim, 1);
      assert.equal(result, true);

      [result, new_sim] = edua.transfer(new_sim, 1, 1, 2);
      assert.equal(result, false);
      checkProductInBuffer(new_sim, 1, 1, 1);
      checkNoProductInBuffer(new_sim, 3, 1);
    });

    it("transfer can be performed", () => {
      let [result, new_sim] = edua.execAction(sim, 1);
      assert.equal(result, true);

      [result, new_sim] = edua.transfer(new_sim, 1, 1, 1);
      assert.equal(result, true);
      checkNoProductInBuffer(new_sim, 1, 1);
      checkProductInBuffer(new_sim, 3, 1, 1);
    });

  });

  describe("single operation", () => {

    let buyProductByActionId = (sim, action_id) => {
      let [result, new_sim] = edua.execAction(sim, 1);
      assert.equal(result, true);
      return new_sim;
    };

    let setUpMode = (sim, wst_id, res_id, mode_id, time) => {
      let [result, new_sim] = edua.assignResource(sim, wst_id, res_id, 1);
      assert.equal(result, true);

      [result, new_sim] = edua.startMode(new_sim, wst_id, mode_id);
      assert.equal(result, true);
      for (let i = 0; i < time; i++)
        new_sim = edua.tick(new_sim);

      checkWstInfoProp(new_sim, wst_id, "machine_mode", mode_id);
      checkWstInfoProp(new_sim, wst_id, "status", "idle");

      return new_sim;
    };
    
    it("without the proper mode, operation cannot be started", () => {
      let result, new_sim;

      // buy a single piece of product 1
      new_sim = buyProductByActionId(sim, 1);

      // try to start the operation 1
      [result, new_sim] = edua.startOperation(new_sim, 1, 1); // wst_id, operation_id

      assert.equal(result, false);
    });

    it("without the needed products, operation cannot be started", () => {
      let result, new_sim;

      // set up the proper mode for operation 1
      new_sim = setUpMode(sim, 1, 1, 1, 240);

      // try to start the operation 1
      [result, new_sim] = edua.startOperation(new_sim, 1, 1); // wst_id, operation_id

      assert.equal(result, false);
    });

    it("operation can be started", () => {
      let result, new_sim;

      // buy a single piece of product 1
      new_sim = buyProductByActionId(sim, 1);

      // transfer it to the workstation's buffer
      [result, new_sim] = edua.transfer(new_sim, 1, 1, 1);
      assert.equal(result, true);

      // set up the proper mode for the operation 1
      new_sim = setUpMode(new_sim, 1, 1, 1, 240);

      // start the operation 1
      [result, new_sim] = edua.startOperation(new_sim, 1, 1); // wst_id, operation_id

      // operation should've been started
      assert.equal(result, true);
      checkWstInfoProp(new_sim, 1, "status", "operation");
      checkWstInfoProp(new_sim, 1, "operation_id", 1);
      checkWstInfoProp(new_sim, 1, "time", 28);
      checkWstInfoProp(new_sim, 1, "totalTime", 28);

      // the product one should've been taken from the buffer
      checkNoProductInWorkstationBuffer(new_sim, 1, 1);
      checkNoProductInWorkstationBuffer(new_sim, 1, 2);
    });

    it("operation can be completed", () => {
      let result, new_sim;

      // perform the operation 1
      new_sim = buyProductByActionId(sim, 1);
      [result, new_sim] = edua.transfer(new_sim, 1, 1, 1);
      assert.equal(result, true);
      new_sim = setUpMode(new_sim, 1, 1, 1, 240);
      [result, new_sim] = edua.startOperation(new_sim, 1, 1); // wst_id, operation_id
      for (let i = 0; i < 28; i++)
        new_sim = edua.tick(new_sim);

      // the workstation should be idle
      checkWstInfoProp(new_sim, 1, "status", "idle");

      // the machine mode should be the same
      checkWstInfoProp(new_sim, 1, "machine_mode", 1);

      // the final product should be in the workstation buffer
      checkProductInWorkstationBuffer(new_sim, 1, 2, 1);
    });

    it("releasing the resource stops the operation without the result products", () => {
      let result, new_sim;

      // start the operation
      new_sim = buyProductByActionId(sim, 1);
      new_sim = setUpMode(new_sim, 1, 1, 1, 240);
      [result, new_sim] = edua.startOperation(new_sim, 1, 1); // wst_id, operation_id

      // release the resource
      [result, new_sim] = edua.releaseResource(new_sim, 1, 1, 1);
      assert.equal(result, true);

      // the workstation should be in no mode
      checkWstInfoProp(new_sim, 1, "status", "not ready");
      checkWstInfoProp(new_sim, 1, "machine_mode", null);

      // the final product should not be in the workstation buffer
      checkNoProductInWorkstationBuffer(new_sim, 1, 2);
    });

  });

  describe("handlers", () => {

    it("invalid handler", () => {
      // invalid action id
      let [result, _id, new_sim] = edua.addBeforeAction(sim, 50, () => false );
      assert.equal(result, false);
      
      // invalid operation id
      [result, _id, new_sim] = edua.addBeforeOperation(new_sim, 50, () => false );
      assert.equal(result, false);

      // invalid transfer id
      [result, _id, new_sim] = edua.addBeforeTransfer(new_sim, 50, () => false );
      assert.equal(result, false);
    });

    it("action", () => {
      // the handler - at most 1 piece of product 1
      let handler = (_assets, b_info, _action_id) => {
        let product_1 = b_info.products.filter( (x) => x.product_id === 1 );
        if ( product_1.length === 0 )
          return true;
        product_1 = product_1[0];
        return product_1.qty < 1;
      };

      // install the handler
      let [result, id, new_sim] = edua.addBeforeAction(sim, 1, handler);
      assert.equal(result, true);

      // the first action should be successfull
      [result, new_sim] = edua.execAction(new_sim, 1);
      assert.equal(result, true);
      checkProductInBuffer(new_sim, 1, 1, 1);

      // the second action should be stopped
      [result, new_sim] = edua.execAction(new_sim, 1);
      assert.equal(result, false);
      checkProductInBuffer(new_sim, 1, 1, 1);

      // remove the handler
      [result, new_sim] = edua.removeHandler(new_sim, id);
      assert.equal(result, true);

      // the third action should be successfull
      [result, new_sim] = edua.execAction(new_sim, 1);
      assert.equal(result, true);
      checkProductInBuffer(new_sim, 1, 1, 2);
    });

    it("operation");

    it("transfer");

    it("tick");

  });

  describe("Visualization", () => {

    let getEntity = (info, type, id) => {
      let entities =
        info.filter( (x) => (x.type === type) && (x.id === id) );
      assert.equal(entities.length, 1);
      return entities[0];
    };

    let checkEntity = (info, type, id, x, y) => {
      let entity = getEntity(info, type, id);
      assert.deepPropertyVal(entity, "x", x);
      assert.deepPropertyVal(entity, "y", y);
    };

    let checkInside = (info, type, id, where, subtype, subid) => {
      let entity = getEntity(info, type, id);
      if ( subtype === undefined ) {
        assert.equal( entity[where].length, 0 );
      } else {
        let inside = entity[where];
        assert.deepInclude(inside, { type: subtype, id: subid });
      }
    };

    it("visInfo", () => {
      let info = edua.visInfo(sim);

      checkEntity(info, "buffer", 1, 1, 2);
      checkInside(info, "buffer", 1, "inputs");
      checkInside(info, "buffer", 1, "outputs", "workstation", 1);
      checkInside(info, "buffer", 1, "outputs", "workstation", 2);

      checkEntity(info, "buffer", 2, 6, 2);
      checkInside(info, "buffer", 2, "outputs");
      checkInside(info, "buffer", 2, "inputs", "workstation", 1);
      checkInside(info, "buffer", 2, "inputs", "workstation", 4);
      checkInside(info, "buffer", 2, "inputs", "workstation", 5);

      checkEntity(info, "workstation", 1, 2, 1);
      checkInside(info, "workstation", 1, "inputs", "buffer", 1);
      checkInside(info, "workstation", 1, "outputs", "buffer", 2);
      checkInside(info, "workstation", 1, "outputs", "workstation", 5);

      checkEntity(info, "workstation", 2, 2, 3);
      checkInside(info, "workstation", 2, "inputs", "buffer", 1);
      checkInside(info, "workstation", 2, "outputs", "workstation", 3);

      checkEntity(info, "workstation", 3, 3, 3);
      checkInside(info, "workstation", 3, "inputs", "workstation", 2);
      checkInside(info, "workstation", 3, "outputs", "workstation", 4);

      checkEntity(info, "workstation", 4, 4, 3);
      checkInside(info, "workstation", 4, "inputs", "workstation", 3);
      checkInside(info, "workstation", 4, "outputs", "workstation", 5);
      checkInside(info, "workstation", 4, "outputs", "buffer", 2);

      checkEntity(info, "workstation", 5, 5, 2);
      checkInside(info, "workstation", 5, "inputs", "workstation", 1);
      checkInside(info, "workstation", 5, "inputs", "workstation", 4);
      checkInside(info, "workstation", 5, "outputs", "buffer", 2);
    });

  });

});
