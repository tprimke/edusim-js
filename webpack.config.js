const path = require('path');

const browser_config = {
  target: 'web',
  entry: './lib/js/src/EduA.bs.js',
  output: {
    filename: 'EduA.js',
    library: 'edua',
    path: path.resolve(__dirname, 'dist')
  }
};

const node_config = {
  target: 'node',
  entry: './lib/js/src/EduA.bs.js',
  output: {
    filename: 'EduA.node.js',
    libraryTarget: 'commonjs2',
    path: path.resolve(__dirname, 'dist')
  }
};

module.exports = [ browser_config, node_config ];

